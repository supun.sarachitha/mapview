﻿using MapView.Data;
using MapView.Model;
using MapView.View;
using Plugin.FilePicker;
using System;
using System.Collections.Generic;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MapView
{
    public partial class App : Application
    {

        public static List<BaseModel> DataStore = new List<BaseModel>();

        public App()
        {

            
            if (Preferences.Get("FirstTime", true) || Preferences.Get("DataList", "") == "[]")
            {
                MockData mock = new MockData();
                mock.AddData();
                Preferences.Set("FirstTime", false);

            }

            MainPage = new NavigationPage(new AuthPage());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
