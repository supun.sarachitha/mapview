﻿using MapView.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace MapView.API
{
    public class BaseMapAPI
    {
        List<BaseModel> data = new List<BaseModel>();
        internal async Task<List<BaseModel>> GetData(string savedlist)
        {
            try
            {
               
                data = JsonConvert.DeserializeObject<List<BaseModel>>(savedlist);
                return data;
            }

            catch (Exception exp)
            {
                return null;
            }
        }
    }
}
