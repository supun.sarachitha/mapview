﻿
using MapView.Model;
using MapView.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace MapView.Data
{
    public class MockData
    {

        public async void AddData()
        {

            App.DataStore = new List<BaseModel>()
            {
                new BaseModel
                {
                    Id=1,
                    PlatformName = "Facebook",
                    ProfilePic = null,
                    UserName  = "User A",
                    Status = "status1",
                    lat = -24.527135,
                    lon=46.679096,
                    DatTime =DateTime.Now.Date.ToString(),
                    Availability=true,

                },
                new BaseModel
                {
                    Id=2,
                    PlatformName = "Google",
                    ProfilePic = null,
                    UserName  = "User B",
                    Status = "status2",
                    lat = 50.513427,
                    lon=29.087356,
                    DatTime =DateTime.Now.Date.ToString(),
                    Availability=true,
                },
                new BaseModel
                {
                    Id=3,
                    PlatformName = "Facebook",
                    ProfilePic = null,
                    UserName  = "User C",
                    Status = "status3",
                    lat = 45.828799,
                    lon=62.627134,
                    DatTime =DateTime.Now.Date.ToString(),
                    Availability=true,
                },
                new BaseModel
                {
                    Id=4,
                    PlatformName = "Google",
                    ProfilePic = null,
                    UserName  = "User D",
                    Status = "status4",
                    lat = 55.677584,
                    lon=-3.582213,
                    DatTime =DateTime.Now.Date.ToString(),
                    Availability=true,
                },
                new BaseModel
                {
                    Id=5,
                    PlatformName = "Facebook",
                    ProfilePic = null,
                    UserName  = "User E",
                    Status = "status5",
                    lat = 45.058001,
                    lon=1.124816,
                    DatTime =DateTime.Now.Date.ToString(),
                    Availability=true,
                },

                 new BaseModel
                {
                    Id=5,
                    PlatformName = "Google",
                    ProfilePic = null,
                    UserName  = "User F",
                    Status = "status6",
                    lat = 17.978733,
                    lon=1.197175,
                    DatTime =DateTime.Now.Date.ToString(),
                    Availability=true,
                },
                 new BaseModel
                {
                    Id=5,
                    PlatformName = "Facebook",
                    ProfilePic = null,
                    UserName  = "User G",
                    Status = "status7",
                    lat = 13.923404,
                    lon=-10.763931,
                    DatTime =DateTime.Now.Date.ToString(),
                    Availability=true,
                },
            };


            await Update(App.DataStore);
        }

        internal async Task<bool> SaveData(CustomPin pin)
        {
            MapViewModel VM = new MapViewModel();
            List<BaseModel> result = await VM.GetData();
            foreach (var item in result)
            {
                if (item.Id == pin.Id)
                {
                    item.Status = pin.Status;
                }
            }

          bool res = await Update(result);
          return res;
        }

        public async Task<bool> Update(List<BaseModel> dataStore)
        {
            string listString = JsonConvert.SerializeObject(dataStore);

            Preferences.Remove("DataList");

            Preferences.Set("DataList", listString);
                                   
            return await Task.FromResult(true); 
        }


    }
}
