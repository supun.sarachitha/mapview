﻿using MapView.API;
using MapView.Data;
using MapView.Model;
using Newtonsoft.Json.Schema;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace MapView.ViewModel
{
    

    public class MapViewModel
    {
        public BaseMapAPI api = new BaseMapAPI();
        MockData mockData = new MockData();
        public async Task<List<BaseModel>> GetData()
        {

            string Savedlist = Preferences.Get("DataList", "");
            List<BaseModel> res = await  api.GetData(Savedlist);

            if (res.Count > 0)
            {
                return res;
            }
            else
            {
                return null;
            }
           
        }

        internal async Task<bool> Save(CustomPin pin)
        {
            bool Validationres = ValidateStatusString(pin.Status);

            bool res = false;
            if (Validationres)
            {
                res = await mockData.SaveData(pin);
            }
            else
            {
                res = false;
            }
          

            return res;
        }

        public bool ValidateStatusString(string status)
        {
            if (string.IsNullOrEmpty(status))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool Validate(BaseModel b)
        {
            if (b.Availability != null)
            {
                if (b.DatTime != null)
                {
                    if (b.Id != null)
                    {
                        if (b.UserName != null)
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }
    }
}
