﻿
using MapView.Model;
using MapView.View;
using MapView.ViewModel;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Serialization;
using Xamarin.Forms;
using Xamarin.Forms.Maps;


namespace MapView
{
    public partial class MapPage : ContentPage
    {
        MapViewModel VM = new MapViewModel();
        List<BaseModel> result;
        public MapPage()
        {
            InitializeComponent();
            LoadData();

            MessagingCenter.Subscribe<App>((App)Application.Current, "PopupClosed", (sender) => {
                LoadData();
            });
        }


        public async void LoadData()
        {
            //get data
            result = await VM.GetData();

            try
            {
                customMap.CustomPins = new List<CustomPin>();
                CustomPin pin = null;

                //add to map
                foreach (var item in result)
                {
                    pin = new CustomPin
                    {
                        Position = new Position(item.lat, item.lon),
                        Label = item.PlatformName,
                        Status = item.Status,
                        Id = item.Id,
                        ImageSource = item.ProfilePic,

                    };

                    customMap.Pins.Add(pin);
                    customMap.CustomPins.Add(pin);
                    pin.MarkerClicked += Pin_MarkerClicked;
                }

                customMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(44.087585, 3.307958), Distance.FromMiles(4000.0)));

            }
            catch (Exception)
            {

                return;
            }
        }

        private async void Pin_MarkerClicked(object sender, PinClickedEventArgs e)
        {
            try
            {
                CustomPin pin = (CustomPin)sender;
                var res = result.Where(x => x.Id == pin.Id);

                await Navigation.PushPopupAsync(new TweetPage(pin));
            }
            catch (Exception)
            {

                return;
            }

        }

        private async void btnAdd_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new WelcomePage());
        }
    }
}
