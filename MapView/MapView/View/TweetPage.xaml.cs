﻿using MapView.ViewModel;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MapView.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TweetPage : PopupPage
    {

        MapViewModel vm = new MapViewModel();
        CustomPin Pin;


        public TweetPage(CustomPin pin)
        {
            InitializeComponent();
            Pin = pin;
            txtStatus.IsReadOnly = true;
            btnSave.IsEnabled = false;
            btnEdit.IsEnabled = true;
            FileImagePreview.Source = ImageSource.FromFile(pin.ImageSource);

            txtStatus.Text = pin.Status;


        }

        private void btnEdit_Clicked(object sender, EventArgs e)
        {
            txtStatus.IsReadOnly = false;
            btnSave.IsEnabled = true;
            btnEdit.IsEnabled = false;
            
        }

        private async void btnDelete_Clicked(object sender, EventArgs e)
        {
            Isbusy.IsVisible = true;
            txtStatus.Text = "";
            txtStatus.IsReadOnly = true;
            btnEdit.IsEnabled = true;
            btnSave.IsEnabled = false;

            Pin.Status = "";
            bool res = await vm.Save(Pin);
            if (res)
            {
                await DisplayAlert("", "Deleted", "Ok");
            }
            Isbusy.IsVisible = false;
        }

        private async void btnClose_Clicked(object sender, EventArgs e)
        {

            MessagingCenter.Send<App>((App)Application.Current, "PopupClosed");
            await Navigation.PopPopupAsync();
        }

        private async void btnSave_Clicked(object sender, EventArgs e)
        {
            Isbusy.IsVisible = true;
            txtStatus.IsReadOnly = true;
            btnEdit.IsEnabled = true;
            btnSave.IsEnabled = false;

            Pin.Status = txtStatus.Text;
            bool res = await vm.Save(Pin);
            if (res)
            {
                await DisplayAlert("", "Saved", "Ok");
            }
            Isbusy.IsVisible = false;

        }



         
    }
}