﻿using MapView;
using MapView.Data;
using MapView.Model;
using MapView.ViewModel;
using Newtonsoft.Json.Linq;
using Plugin.FacebookClient;
using Plugin.FilePicker;
using Plugin.FilePicker.Abstractions;
using Plugin.Geolocator;
using Plugin.GoogleClient;
using Plugin.GoogleClient.Shared;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace MapView.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WelcomePage : ContentPage
    {
        public static string LoginType;
        GoogleUser _googleUser;
        string Propic = null;

        decimal latitude;
        decimal longitude;

        MapViewModel VM = new MapViewModel();

        public WelcomePage()
        {
            InitializeComponent();

            latitude = 0; longitude = 0;

            ContactMap.MapClicked += ContactMap_MapClicked;
            //gdBusy.IsVisible = true;
            //_googleUser = data;

            GetLocation();
        }

        private void ContactMap_MapClicked(object sender, MapClickedEventArgs e)
        {
            latitude = Convert.ToDecimal(ContactMap.VisibleRegion.Center.Latitude);
            longitude = Convert.ToDecimal(ContactMap.VisibleRegion.Center.Longitude);

            

        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();

        }



        private async void btnAdd_Clicked(object sender, EventArgs e)
        {

            try
            {

               


                    latitude = Convert.ToDecimal(ContactMap.VisibleRegion.Center.Latitude);
                    longitude = Convert.ToDecimal(ContactMap.VisibleRegion.Center.Longitude);


                MapViewModel VM = new MapViewModel();
                List<BaseModel> result = await VM.GetData();
                var I = result.OrderByDescending(X => X.Id).FirstOrDefault();

                BaseModel B = new BaseModel
                {
                    Id = I.Id + 1,
                    PlatformName = RadF.IsChecked ? "Facebook" : RadG.IsChecked ? "Google" : "Facebook",
                    Availability = RadAvailable.IsChecked ? true : RadUnavailable.IsChecked ? false: false,
                    ProfilePic = Propic,
                    UserName = lblName.Text,
                    Status = lblStatus.Text,
                    Services = lblServices.Text,
                    lat = Convert.ToDouble(latitude),
                    lon = Convert.ToDouble(longitude),
                    DatTime = DateTime.Now.Date.ToString()
                };


                bool Val = VM.Validate(B);

                if (!Val)
                {
                    await DisplayAlert("","Invalid Data","Ok");
                    return;
                }

                result.Add(B);
                MockData M = new MockData();
                bool res = await M.Update(result);

                if (res)
                {
                    await DisplayAlert("", "New Collaborator Saved", "Ok");
                    
                    await App.Current.MainPage.Navigation.PopAsync();
                }
            }
            catch (Exception ex)
            {

                return;
            }

        }

        

        private async void btnLocation_Clicked(object sender, EventArgs e)
        {
            GetLocation();
        }

        private async void GetLocation()
        {
            try
            {
                var location = await Geolocation.GetLastKnownLocationAsync();
                latitude = Convert.ToDecimal(location.Latitude);
                longitude = Convert.ToDecimal(location.Longitude);
                ContactMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(location.Latitude, location.Longitude), Distance.FromMiles(5000.0)));

            }
            catch (Exception ex)
            {

                return;
            }
        }

        protected override void OnDisappearing()
        {
            MessagingCenter.Send<App>((App)Application.Current, "PopupClosed");
            base.OnDisappearing();
        }


        public async void  PickAndShow()
        {
            try
            {

                string[] fileTypes = null;
                if (Device.RuntimePlatform == Device.Android)
                    fileTypes = new string[] { "image/png", "image/jpeg" };

                if (Device.RuntimePlatform == Device.iOS)
                    fileTypes = new string[] { "public.image" };

                FileData fileData = await CrossFilePicker.Current.PickFile(fileTypes);
                if (fileData == null)
                    return; // user canceled file picking

                string fileName = fileData.FileName;
                //string contents = System.Text.Encoding.UTF8.GetString(fileData.DataArray);

                if (fileData.FileName.EndsWith("jpg", StringComparison.OrdinalIgnoreCase)
                        || fileData.FileName.EndsWith("png", StringComparison.OrdinalIgnoreCase))
                {
                    FileImagePreview.Source = ImageSource.FromStream(() => fileData.GetStream());
                    FileImagePreview.IsVisible = true;

                    Propic = fileData.FilePath;

                    FileImagePreview.Source = ImageSource.FromFile(fileData.FilePath);
                }
                else
                {
                    FileImagePreview.IsVisible = false;
                }

            }
            catch (Exception ex)
            {
                System.Console.WriteLine("Exception choosing file: " + ex.ToString());
            }
        }

        private void btnupload_Clicked(object sender, EventArgs e)
        {
            PickAndShow();
        }
    }
}