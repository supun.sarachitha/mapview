﻿
using Plugin.FilePicker.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MapView.Model
{
    public class BaseModel
    {
        public string PlatformName { get; set; }

        public int Id { get; set; }
        public string UserName { get; set; }
        public string ProfilePic { get; set; }
        public string Status { get; set; }

        public string Services { get; set; }

        public bool Availability { get; set; } = false;
        public double lon { get; set; }
        public double lat { get; set; }
        public string DatTime { get; set; }
    }






}
