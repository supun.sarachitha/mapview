﻿using Xamarin.Forms.Maps;

namespace MapView
{
    public class CustomPin : Pin
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string ImageSource { get; set; }
        public string Status { get; set; }
        public string Url { get; set; }
    }
}
