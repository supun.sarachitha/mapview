﻿using System;
using MapView.ViewModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest3
    {
        [TestMethod]
        public void StatusStringValidation3() //test for Correct value pass
        {
            MapViewModel mapViewModel = new MapViewModel();

            bool expectedresult = true;
            bool actualResult = mapViewModel.ValidateStatusString("Im available Until 2pm");

            Assert.AreEqual(expectedresult, actualResult);


        }

    }
}
