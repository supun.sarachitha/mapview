﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MapView;
using MapView.Model;
using MapView.View;
using MapView.ViewModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Xunit;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest4
    {
        [TestMethod]
        public void ValidateSaveCollaborator()
        {
            MapViewModel newColloborator = new MapViewModel();

            BaseModel B = new BaseModel
            {
                Id = 10,
                PlatformName = "Facebook",
                Availability = true,
                ProfilePic = "",
                UserName = "Jack",
                Status = "Available for service",
                Services = "",
                lat = 0,
                lon = 0,
                DatTime = DateTime.Now.Date.ToString()
            };

            bool  res =  newColloborator.Validate(B);




            Assert.IsTrue(res);
        }
    }
}
